var grid; 
var rows = 20; 
var columns = 20;
var w = 20;
var targetMines = 30;

function makeArray (rows, colums) {
  var array = new Array(rows); 

  for (var i  = 0; i < array.length; i++) {
      array[i] = new Array(colums);
  }
  return array; 
}

function setup () {

  createCanvas(405,405); 
  cols = floor(width / w); 
  rows = floor(height / w);

  grid = makeArray(rows, columns);
  for (var i = 0; i < columns; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j] = new Cell(i, j, w);// initializing an array where each item is a cell
    }
  }

  var currentMines = 0;
// placing the mines on the grid
  do {
    rand1 = floor(random(rows));
    rand2 = floor(random(columns));
    grid[rand1][rand2].bee = true;
    currentMines++;
  }
  while ( targetMines > currentMines );


  // count neighbours
  for (var i = 0; i < columns; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j].countBees(); 
    }
  }

}

function mousePressed() {
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      if (grid[i][j].contains(mouseX, mouseY)) {
        grid[i][j].reveal();

        // if a mine is clicked, game over, open all cells 
        if (grid[i][j].bee == true) {
          for (var i = 0; i < cols; i++) {
            for (var j = 0; j < rows; j++) {
              grid[i][j].reveal();
            }
          }
        }
      }
    }
  }
}


function draw() {

  // background(0);
  for (var i = 0; i < columns; i++) {
    for (var j = 0; j < rows; j++) {
      grid[i][j].show();

    }
  }
}
