class Cell {
    constructor(i, j, width) {
        this.i = i;
        this.j = j;
        this.x = i * width;
        this.y = j * width;
        this.bee = false;
        this.revealed = true;
        this.width = this.height = width;
        this.revealed = false;
        this.neighbourCount = 0;
    }
    show() {
        // fill(color("LightGray"));
        stroke(0);
        fill(color("Gainsboro"));
        rect(this.x, this.y, this.width, this.height);
        if (this.revealed) {
            if (this.bee) {
                // this.neighbourCount = -1;
                fill(color("Black"));
                ellipse(this.x + this.width * 0.5, this.y + this.width * 0.5, this.width * 0.5);
            }
            else if (this.neighbourCount > 0) {
                textAlign(CENTER); // not working, don't know why. function from p5.js library. https://p5js.org/reference/#/p5/textAlign
                fill(0);
                text(this.neighbourCount, this.x + this.width * 0.5, this.y + this.width * 0.75); // trial and error to center the text
            }
            else {
                fill(color("White"));
                rect(this.x, this.y, this.width, this.height);
            }
        }
    }
    contains(x, y) {
        return (x > this.x && x < this.x + this.width) && (y > this.y && y < this.y + this.width);
        // used to detect if mouse pointer is within the boundaries of one cell
    }
    reveal() {
        this.revealed = true;
        if (this.neighbourCount == 0) {
            for (var xoff = -1; xoff <= 1; xoff++) {
                var i = this.i + xoff;
                if (i < 0 || i >= cols)
                    continue;
                for (var yoff = -1; yoff <= 1; yoff++) {
                    var j = this.j + yoff;
                    if (j < 0 || j >= rows)
                        continue;
                    grid[i][j].revealed = true;
                    // grid[i][j].reveal(); throws maximum call stack size exceeded 
                }
            }
        }
    }
    countBees(x, y) {
        var total = 0; // local var to keep count
        if (this.bee) {
            this.neighbourCount = -1;
            return;
        }
        for (var xoff = -1; xoff <= 1; xoff++) {
            var i = this.i + xoff;
            if (i < 0 || i >= cols)
                continue;
            for (var yoff = -1; yoff <= 1; yoff++) {
                var j = this.j + yoff;
                if (j < 0 || j >= rows)
                    continue;
                let cell = grid[i][j];
                if (cell.bee) {
                    total++;
                }
            }
        }
        return this.neighbourCount = total;
    }
}





